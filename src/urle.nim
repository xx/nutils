import std/strutils
import lib/[cmdline, prnt]

let (_, params) = readInput()

for line in params:
  var res = ""
  for c in line:
    case c:
      of 'a'..'z', 'A'..'Z', '0'..'9',
        '-', '.', '_', '~', '!', '*', '\'', '(', ')':
        res.add c
      else:
        res.add '%'
        res.add toHex(c.ord, 2)

  if params.len == 1:
    prnt res
  else:
    echo res
