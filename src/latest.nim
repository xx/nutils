import std/[os, posix, posix_utils]
import lib/prnt

proc `<`(t1, t2: Time): bool {.borrow.}

var
  params = commandLineParams()
  latest: (string, Time)

if params.len < 1:
  params = @[getCurrentDir()]

for dir in params:
  for file in walkDir dir:
    let
      s = stat file.path
      tim =
        when StatHasNanoseconds: s.st_mtim.tv_sec
        else: s.st_mtime
    if latest[1] < tim:
      latest = (file.path, tim)

prnt latest[0]
