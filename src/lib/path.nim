import std/[envvars, strutils, os]

proc locate*(name: string): string =
  ## Returns the full path of the specified binary.
  for dir in getEnv("PATH").split(":"):
    for file in dir.walkDir(relative = true):
      if file.path == name:
        return dir & "/" & file.path

when isMainModule:
  doAssert locate("env") == "/usr/bin/env"
  doAssert locate("doesnt_exist") == ""
