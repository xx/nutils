import std/strutils
import lib/[cmdline, prnt]

let (_, params) = readInput()

for line in params:
  var res = newString(line.len * 3)

  for i, c in line.pairs:
    let hex = toHex(c.ord, 2)
    res[i * 3] = '%'
    res[i * 3 + 1] = hex[0]
    res[i * 3 + 2] = hex[1]

  if params.len == 1:
    prnt res
  else:
    echo res
