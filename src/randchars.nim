import std/[os, random, strutils, sugar]
import lib/prnt

const selection = Digits + Letters

let
  params = commandLineParams()
  length =
    if params.len < 1: 16
    else: parseInt params[0]

randomize()

let res = collect:
  for _ in 0 ..< length:
    selection.sample

prnt cast[string](res)
