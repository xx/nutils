import std/base64
import lib/cmdline

let (_, params) = readInput()

for decoded in params:
  stdout.write decoded.encode
  if params.len != 1: stdout.write "\n"
  stdout.flushFile
