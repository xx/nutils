import std/os
import lib/[path, procs]

var bat = locate "bat"
if bat == "": bat = "cat"

for path in commandLineParams():
  let bin = locate path
  if bin == "":
    echo "no such binary: " & path
    continue
  echo bat.exec ["-f", bin]
