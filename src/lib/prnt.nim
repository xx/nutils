import std/terminal

let isTty = stdout.isatty

proc prnt*(what: string) =
  ## Writes to stdout.
  ## If the stdout is a tty, appends a newline.
  ## Otherwise, no newline is inserted.
  stdout.write what
  if isTty: stdout.write "\n"
  stdout.flushFile
