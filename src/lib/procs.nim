import std/osproc

proc exec*(bin: string, args: openArray[string]): string =
  bin.execProcess(args = args, options = {poUsePath, poStdErrToStdOut})
