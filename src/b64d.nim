import std/[base64, strutils]
import lib/cmdline

var (hasParams, params) = readInput()

if not hasParams:
  params = @[params[0].replace("\n", "")]

for encoded in params:
  stdout.write encoded.decode
  if params.len != 1: stdout.write "\n"
  stdout.flushFile
