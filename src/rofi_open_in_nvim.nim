import std/[os, osproc, sugar, streams, strutils]
import lib/procs

let
  params = commandLineParams()
  dir =
    if params.len < 1: getCurrentDir()
    else: params[0]

setCurrentDir dir

let
  windowTitle = lastPathPart dir
  files = collect(for f in walkDir(".", relative = true): f.path).join "\n"

  rofi = startProcess(
    "rofi", args = ["-dmenu", "-sync", "-p", windowTitle], options = {poUsePath}
  )

rofi.inputStream.write files
rofi.inputStream.flush
rofi.inputStream.close

let selection = rofi.outputStream.readLine
rofi.close

echo exec("systemd-run", args = ["--user", "--", "footclient", "-D", dir / selection, "--", "nvim", "."])
