# Package

version       = "0.1.0"
author        = "Rasmus Moorats"
description   = "Helper utils I use in the terminal"
license       = "MIT"
srcDir        = "src"
bin           = @["b64d",
                  "b64e",
                  "glog",
                  "latest",
                  "randchars",
                  "rofi_open_in_nvim",
                  "rofi_calc",
                  "saved",
                  "urle",
                  "urle_all",
                  "urld",
                  "bcat"]
binDir        = "bin"


# Dependencies

requires "nim >= 2.0.0"
requires "https://git.dog/xx/nim-swayipc.git#head"
