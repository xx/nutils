import std/strutils
import lib/[cmdline, prnt]

let (_, params) = readInput()

for line in params:
  var res = ""
  var i = 0
  while i < line.len:
    case line[i]:
      of '%':
        res.add chr(fromHex[uint8](line[i+1 .. i+2]))
        i += 2
      of '+':
        # todo: figure out if this special case is needed
        # currently, I don't encode ' ' as '+' in urle.nim
        res.add ' '
      else:
        res.add line[i]
    inc i

  if params.len == 1:
    prnt res
  else:
    echo res
