import os

proc readInput*(): (bool, seq[string]) =
  ## If command line params are given, return those.
  ## Otherwise, return the entirety of stdin as the first element in the seq.
  ## If read from stdin, bool is set to false, otherwise true.
  let params = commandLineParams()
  if params.len > 0: return (true, params)

  return (false, @[stdin.readAll()])
