import std/[os, strformat, strutils]
import lib/procs

let
  gitArgs = @[
    "log",
    "--color",
    "--graph",
    "--pretty=format:%G? %C(dim)%h%Creset %s %Cgreen(%cr)%C(yellow)%d%C(blue) <%an>%Creset",
    "--abbrev-commit"
  ] & commandLineParams()

  rawOutput = "git".exec gitArgs

for line in rawOutput.splitLines(keepEol = true):
  let
    keys = line.split(" ", maxsplit = 2)
  if keys.len < 3 or keys[0] != "*":
    stdout.write line
    stdout.flushFile
    continue

  let col = case keys[1]:
    of "B": "31"   # bad
    of "G": "1;32" # good
    of "U": "32"   # good, unknown
    of "X": "32"   # good, expired
    of "Y": "33"   # good, expired key
    of "R": "33"   # good, revoked key
    of "E": "33"   # missing key
    of "N": "2"    # no signature
    else: "0"      # this shouldn't really happen

  stdout.write &"* \x1B[{col}m{keys[1]}\x1B[0m {keys[2]}"
  stdout.flushFile
