import std/osproc

let rofi = startProcess(
    "rofi",
    args = ["-show", "calc", "-modi", "calc", "-no-show-match", "-calc-command-history",
      "-no-sort", "-calc-command", "printf '%s' '{result}' | wl-copy"],
    options = {poUsePath}
  )

rofi.close
